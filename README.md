A guided handbook on how to use [Digital Rebar Provision (DRP)](http://rebar.digital/). The handbook is open to the world, and we welcome feedback. Please make a merge request to suggest improvements or add clarifications. Please use issues to ask questions.

## Digital Rebar Provision

<img src="https://provision.readthedocs.io/en/tip/_images/dr_provision.png" height="200"/>

[Digital Rebar Provision (DRP)](http://rebar.digital/) is the open source data center provisioning system with a Cloud Native Architecture replacing Cobbler, Foreman, MaaS or similar technologies. It provides API-driven DHCP/PXE/TFTP provisioning and workflow system and distributed as a single Golang binary with no dependencies capable of installation on a Linux, macOS or Raspberry Pi.

**Key Features**

- API-driven Infrastructure-as-Code automation
- Event driven actions via Websockets API
- Extensible Plug-in Model for enhancements
- Supports orchestration tools like Chef, Ansible, Puppet, SaltStack, Teraform, etc
- RAID, IPMI, and BIOS Configuration (via commercial plugins)

## Requirements

In order to spin up virtual environment, you will need VirtualBox & Vagrant:

```
$ brew cask install virtualbox virtualbox-extension-pack
$ brew cask install vagrant vagrant-manager
```

## Network Layout

Vagrant has some [hard requirements]((https://www.vagrantup.com/docs/virtualbox/boxes.html#virtual-machine)) for virtual machine created in VirtualBox, thence first network interface (Adapter 1) must be a [NAT adapter](https://www.virtualbox.org/manual/ch06.html#network_nat). For [Private Networks](https://www.vagrantup.com/docs/networking/private_network.html) and [Public Networks](https://www.vagrantup.com/docs/networking/public_network.html) Vagrant uses Host Only Adapter and Bridged Adapter, respectively.

## Notes

The default credentials used for administering the `dr-provision` service is

```
username: rocketskates
password: r0cketsk8ts
```

## Resources

- [Documentation](http://provision.readthedocs.io/en/stable/) ([sources](https://github.com/digitalrebar/provision/tree/master/doc))
- [Issues and Features](https://github.com/digitalrebar/provision/issues)
- [Slack](https://www.rackn.com/support/slack/) ([archive](http://rebar.digital/slack/html/index.html))
- [Gitter Chat](https://gitter.im/digitalrebar/)
